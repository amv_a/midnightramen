window.onload = function () {
    document.getElementById("area").focus()
    document.getElementById("submitBug").onclick = checkRequired
    document.getElementById("capture").onchange = checkAllowed
}

//This method will check that those fields that must be filled are.
function checkRequired(theEvent) {

    var event = theEvent || window.event


    var section = document.getElementById("area")
    var comment = document.getElementById("comment")

    /*This will check if the comment textarea and the select has a valid item selected, if not 
   an alert will be displayed and the fields highligthed*/
    if (comment.value == "") {
        event.preventDefault()
        event.returnValue = false
        alert('El campo descirpción debe ser rellenado.')
        comment.style.border = "2px solid #ff54dc"
    }
    
    if (section.value == "En que sección está el error...") {
        event.preventDefault()
        event.returnValue = false
        alert('El campo de sección debe ser rellenado.')
        section.style.border = "2px solid #ff54dc"
    }
}

function checkAllowed() {

    var ext = this.value.match(/\.([^\.]+)$/)[1]

    switch (ext) {
        case "png":
        case "jpg":
        case "jpeg":
            break

        default:
            this.value = ''
            alert("La extensión de este archivo no está permitida.")
            break

    }
}
