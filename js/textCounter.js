$('div').on('focus keypress', '.comment', function (e) {
    
    var $this = $(this);
    var msgSpan = $this.parents('div').find('.counter_msg');
    var ml = parseInt($this.attr('maxlength'), 10);
    var length = this.value.length;
    var msg = ml - length + ' characters of ' + ml + ' characters left';

    msgSpan.html(msg);
});
