//With this part of code I prepare the hour so it has a 0 before the numbers lower than 10.
function prepTime() {
    var hours = new Date().getHours().toString()
    var minutes = new Date().getMinutes().toString()
    if (hours < 10) {
        hours = "0" + hours
    }
    if (minutes < 10) {
        minutes = "0" + minutes
    }
    var actualHour = hours.concat(minutes)
    return actualHour
}

//This will change the theme of the webpage depending on the hour of the day.
function changeTheme() {

    var actualHour = prepTime()
    
    //This check if it´s the index page because the layout an css is different.
    var path = window.location.pathname
    var page = path.split("/").pop()

    if (page == 'index.php' || page == '') {
        if (actualHour >= 0801 && actualHour <= 1800) {
            document.getElementById("style").href = "../css/midnightRamenDay.min.css"
        } else if (actualHour >= 1801 && actualHour <= 2000) {
            document.getElementById("style").href = "../css/midnightRamenNight.min.css"
        }
    }else {
        if (actualHour >= 0801 && actualHour <= 1800) {
            document.getElementById("style").href = "../css/midnightRamenPagesDay.min.css"
        } else if (actualHour >= 1801 && actualHour <= 2000) {
            document.getElementById("style").href = "../css/midnightRamenPagesNight.min.css"
        }
    }
}

//I set this timer so the page checks the hour each second and it is not necesary to refresh the page
setTimeout(changeTheme(), 1000) 