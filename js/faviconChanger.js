/*This method checks which navigator we are navigating on, using "user agent" which gives us a reference
including the version of the navigator*/
function whichNavigator() {

    //This first if will check if we are in index.hmtl to use a diferent path.
        //We check the string "OPR" because Opera navigator might appear in both forms.
        if ((navigator.userAgent.indexOf("Opera") || navigator.userAgent.indexOf('OPR')) != -1) {
            document.getElementById('faviconShort').href = 'favicon.ico'

        } else if (navigator.userAgent.indexOf("Chrome") != -1) {
            document.getElementById('faviconShort').href = 'faviconNoAnim.ico'

        } else if (navigator.userAgent.indexOf("Safari") != -1) {
            document.getElementById('faviconShort').href = 'favicon.ico'

        } else if (navigator.userAgent.indexOf("Firefox") != -1) {
            document.getElementById('faviconShort').href = 'favicon.ico'

            /*We check documentMode because this method is exclusive to IE, it returns a number (from 5 to the
            to the latest version) and we transform that output into a boolean using !!, so if the string provided
            does not match this extra option will not fail*/
        } else if ((navigator.userAgent.indexOf("Windows") != -1) || (!!document.documentMode == true)) {
            document.getElementById('faviconShort').href = 'faviconNoAnim.ico'

        } else {
            document.getElementById('favicon').href = 'faviconNoAnim.ico'
            document.getElementById('faviconShort').href = 'faviconNoAnim.ico'
        }
}

whichNavigator()
