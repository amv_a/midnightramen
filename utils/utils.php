<?php

function pageRefSelector()
{
    $path = basename($_SERVER["PHP_SELF"]);

    if ($path == "index.php") {

        //Custom style for the index.
        echo "<link id='style' type='text/css' href='css/midnightRamenNight.min.css' rel='stylesheet'>";

        //Custom script for the index.
        echo "<script src='js/grayscale.js'></script>";
    } else {
        //Custom style for the rest of the pages.
        echo "<link id='style' href='../css/midnightRamenPagesNight.min.css' rel='stylesheet'>";
        switch ($path) {
            case "errores.php":
                //Customs script to handle the form validation 
                echo "<script src='../js/formValidate.js'></script>";
                break;
        }
    }
}

function navIcon()
{
    $path = basename($_SERVER["PHP_SELF"]);
    if ($path != "index.php") {
        echo  "<a class='navbar-brand' href='../index.php'><img src='../logoSmall.png'
            alt='Midnight Ramen Logo'></img></a>";
    }
}
?>