<?php
function loadPDF()
{
    // Store the file name into variable 
    $file = '../media/stories/prueba.pdf';
    $filename = 'prueba.pdf';

    // Header content type 
    header('Content-type: application/pdf');

    header('Content-Disposition: inline; filename="' . $filename . '"');

    header('Content-Transfer-Encoding: binary');

    header('Accept-Ranges: bytes');

    // Read the file 
    @readfile($file);
}
