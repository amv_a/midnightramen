<?php
include __DIR__ . "/partials/ini-doc.part.php";

//JS that changes the images of the header and send section
echo "<script src='js/changeTheme.js'></script>";

include __DIR__ . "/partials/nav.part.php";
?>
<!-- Form Section -->
<section id="projects" class="projects-section bg-light">
  <div class="container">
    <img class="img-fluid mb-3 mb-lg-0" src="../media/titles/bugs-title.png" alt="Titulo: Bugs">
    <hr>
    <!-- Form Bugs Row -->
    <div class="row align-items-center no-gutters mb-4 mb-lg-5">
      <div class="col-xl-8 col-lg-7">
        <!-- Section bug-->
        <form action="" id="errors">
          <div class="form-group">
            <label for="area">¿En qué seccion se encuentra el error:</label>
            <select class="form-control" id="area">
              <option>En que sección está el error...</option>
              <option>Página Principal</option>
              <option>Glosario</option>
              <option>Galería</option>
              <option>Versiones</option>
              <option>Futuras Actualizaciones</option>
              <option>Sugerencias y Errores</option>
              <option>Juego</option>
            </select>
          </div>
          <!-- Capture-->
          <div class="form-group">
            <label for="capture">Agregar captura del error.</label>
            <input type="file" id="capture" name="capture">
          </div>
          <!-- Description textarea-->
          <div class="form-group">
            <label for="comment">Descirbe el error brevemente:</label>
            <br />
            <textarea class="form-control rounded-0 comment" maxlength="200" name="comment" id="comment"></textarea>
            <br /> <span style="font-family: 'Varela Round'; 11px sans-serif;color:#4f837f;">
              <span class='counter_msg'></span>
            </span>
          </div>
          <button id="submitBug" class="btn btn-primary">Enviar</button>
        </form>
      </div>
    </div>



</section>

<?php
include __DIR__ . "/partials/end-doc.part.php";
?>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="../js/textCounter.js"></script>

<!-- Custom scripts for this template -->
<script src="../js/grayscale.min.js"></script>

</body>

</html>