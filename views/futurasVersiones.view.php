<!DOCTYPE html>
<html lang="en">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="title" content="Midnight Ramen Game">
  <meta name="description" content="Text based navigator game">
  <meta name="keywords"
    content="game, navigator game, turn-based combat, text-based wiki, juego, juego de navegador, combate por turnos, juego basado en texto">
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="English">
  <meta charset="UTF-8">
  <link rel="icon" id="favicon" href="../favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" id="faviconShort" href="../favicon.ico" type="image/x-icon" />
  <meta name="Aitor Magnieto Ventura" content="">

  <title>Errores</title>

  <!-- Bootstrap core CSS -->
  <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="../vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="../https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link
    href="../https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template -->
  <link id="style" href="../css/midnightRamenPagesNight.min.css" rel="stylesheet">

  <!-- Form Validation JS-->
  <script src="../js/formValidate.js"></script>
  <!-- JS to check which navigator its being used-->
  <script src="../js/faviconChanger.js"></script>
  <!-- JS that changes the images of the header and send section -->
  <script src="../js/changeTheme.js"></script>

</head>

<body id="page-top">

  <!-- Navigation -->
  <nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse"
        data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false"
        aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <a class="navbar-brand" href="../index.html"><img src="../logoSmall.png" alt="Midnight Ramen Logo"></img></a>
        <ul class="navbar-nav mx-auto">
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" id="mainPage" href="../index.html">Página Principal</a>
            </li>
            <li class="nav-item ">
                <a class="nav-link js-scroll-trigger" id="wiki" href="glosario.html">Glosario</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" id="gallery" href="galería.html">Galería</a>
            </li>
            <li class="nav-item active">
                <a class="nav-link js-scroll-trigger" id="toCome" href="futurasVersiones.html">Futuras Actualizaciones</a>
            </li>
            <li class="nav-item">
                <a class="nav-link js-scroll-trigger" id="bugs" href="errores.html">Errores</a>
            </li>
        </ul>
      </div>
    </div>
  </nav>

  <!-- Form Section -->
  <section id="projects" class="projects-section bg-light">
      <div class="container">
        
          </div>
        </div>



  </section>

  <!-- Signup Section -->
  <section id="signup" class="signup-section">
    <div class="container">
      <div class="row">
        <div class="col-md-10 col-lg-8 mx-auto text-center">

          <i class="far fa-paper-plane fa-2x mb-2 text-white"></i>
          <h2 class="text-white mb-5">Subscribete para recibir actualizaciones!</h2>

          <form class="form-inline d-flex">
            <input type="email" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" id="inputEmail"
              placeholder="Introduce tu dirección de email...">
            <button type="submit" id="submit" class="btn btn-primary mx-auto">Subscribe</button>
          </form>

        </div>
      </div>
    </div>
  </section>

  <!-- Contact Section -->
  <section class="contact-section bg-black">
    <div class="container">

      <div class="social d-flex justify-content-center">
        <a href="#" class="mx-2">
          <i class="fab fa-twitter"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fab fa-youtube"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fab fa-github"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fas fa-envelope"></i>
        </a>
        <a href="#" class="mx-2">
          <i class="fa fa-twitch"></i>
        </a>
        <a href="https://www.youtube.com/watch?v=-bzWSJG93P8" class="mx-2">
          <i class="fa fa-ge"></i>
        </a>
      </div>
    </div>

  </section>

  <!-- Footer -->
  <footer class="bg-black small text-center text-white-50">
    <div class="container">
      <p>Las imagenes que aparecen en esta web son creative commons.</p>
      Copyright &copy; Midnight Ramen 2019
    </div>
  </footer>

  <!-- Bootstrap core JavaScript -->
  <script src="../vendor/jquery/jquery.min.js"></script>
  <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Plugin JavaScript -->
  <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for this template -->
  <script src="../js/grayscale.min.js"></script>

</body>

</html>