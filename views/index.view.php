<?php
  include __DIR__."/partials/ini-doc.part.php";
  
  //JS that changes the images of the header and send section
  echo "<script src='js/changeTheme.js'></script>";

  include __DIR__."/partials/nav.part.php";
?>
  <!-- Header Start -->
  <header class="masthead" id="masthead">
    <div class="container d-flex h-100 align-items-center">
      <div class="mx-auto text-center">
        <a id="linkLogo" href="index.html"><img id="imgLogo" class="mx-auto d-block img-fluid" src="logo.png" alt="Midnight Ramen Logo"></a>
        <a id ="play" href="#about" class="btn btn-primary js-scroll-trigger">Juega Ahora</a>
      </div>
    </div>
  </header>
  <!-- Header End -->
  <!-- About Section Start -->
  <section id="intro" class="about-section text-center">
    <div class="container">
      <div class="row">
        <div class="col-lg-8 mx-auto">
          <h2 class="text-white mb-4"><img id="imgLogo" class="mx-auto d-block img-fluid" src="media/titles/description-title.png" alt="Midnight Ramen Logo"></h2>
          <p class="text-white-50">Es un juego text-based y de rol por turnos con un universo de ciencia ficción que tiene 
            lugar en una ciudad enorme semi-futurista donde la gente diferente es invsible a ojos de la sociedad, 
            sigue la historia del protagonista y decide su camino.</p>
        </div>
      </div>
    </div>
  </section>
  <!-- About Section End -->
  <!-- Projects Section Start -->
  <section id="projects" class="projects-section bg-light">
    <div class="container">
      <!-- Main Content Row Start -->
      <div class="row align-items-center no-gutters mb-4 mb-lg-5">
        <div class="col-xl-8 col-lg-7">
          <img class="img-fluid mb-3 mb-lg-0" src="media/backgrounds/game.jpg" alt="Una foto para la descripcion del juego">
        </div>
        <div class="col-xl-4 col-lg-5">
          <!-- Main Content Description -->
          <div class="featured-text text-center text-lg-left">
              <img class="img-fluid mb-3 mb-lg-0" src="media/titles/game-title.png" alt="Titulo: El juego">
            <p class="text-black-50 mb-0">Enfrentate a una socidedad decadente y ciega, mas centrada en las apariencias y la extravagancia que en la vida de las personas.</p>
          </div>
        </div>
      </div>
      <!-- Main Content Row End -->
      <!-- Wiki Row Start -->
      <div class="row justify-content-center no-gutters mb-5 mb-lg-0">
        <div class="col-lg-6">
          <img class="img-fluid" src="media/backgrounds/wiki.jpg" alt="Una foto para la descripción del glosario">
        </div>
        <div class="col-lg-6">
          <div class="bg-black text-center h-100 project">
            <div class="d-flex h-100">
              <div class="project-text w-100 my-auto text-center text-lg-left">
                  <img class="img-fluid mb-3 mb-lg-0" src="media/titles/wiki-title.png" alt="Titulo: Glosario">
                <p class="mb-0 text-white-50">Descubre mas sobre el juego y su mundo, sus personajes, la sociedad y acontecimientos.</p>
                <hr class="d-none d-lg-block mb-0 ml-0">
              </div>
            </div>
          </div>
        </div>
      </div>
      <!-- Wiki Row End -->
      <!-- Gallery Row Start -->
      <div class="row justify-content-center no-gutters">
        <div class="col-lg-6">
          <img class="img-fluid" src="media/backgrounds/gallery.jpg" alt="Una foto para la descripcion de la galería de imagenes">
        </div>
        <div class="col-lg-6 order-lg-first">
          <div class="bg-black text-center h-100 project">
            <div class="d-flex h-100">
              <div class="project-text w-100 my-auto text-center text-lg-right">
                  <img class="img-fluid mb-3 mb-lg-0" src="media/titles/gallery-title.png" alt="Titulo: Galería">
                <p class="mb-0 text-white-50">Aquí podrás encontrar los lugares en los que tiene lugar la historia y imágenes de los personajes y enemigos del juego.</p>
                <hr class="d-lg-block mb-0 mr-0">
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
  <!-- Projects Section End -->

  <?php
    include __DIR__."/partials/end-doc.part.php";
  ?>

  <!-- Bootstrap core JavaScript -->
  <script src="/vendor/jquery/jquery.js"></script>
  <script src="/vendor/bootstrap/js/bootstrap.bundle.js"></script>

  <!-- Plugin JavaScript -->
  <script src="/vendor/jquery-easing/jquery.easing.js"></script>

   <!-- Custom scripts for this template -->
   <script src="js/grayscale.min.js"></script>
  
</body>
</html>
