<?php
  include __DIR__."/partials/ini-doc.part.php";

  //JS that changes the images of the header and send section
  echo "<script src='js/changeTheme.js'></script>";
  
  include __DIR__."/partials/nav.part.php";
?>
    <!-- Gallery Section Start -->
    <section id="projects" class="projects-section bg-light">
    <div class="container">
        <img class="img-fluid mb-3 mb-lg-0" src="../media/titles/gallery.png" alt="Titulo: Galería">
        <hr>
	    <div class="row">
            <!-- Gallery Row Start -->
		    <div class="row">
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="../media/gallery/bg-masthead.jpg" data-image-id="" data-toggle="modal" data-title="https://pixabay.com/users/ethan_zhan-9583454" 
                        data-image ="../media/gallery/bg-masthead.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/bg-masthead.jpg"
                            alt="Fondo del encabezado para la noche">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="../media/gallery/bg-masthead_day.jpg" data-image-id="" data-toggle="modal" data-title="https://pixabay.com/es/users/dylanagonzales2011-13737474/"
                        data-image="../media/gallery/bg-masthead_day.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/bg-masthead_day.jpg"
                            alt="Fondo del encabezado para el día">
                    </a>
                </div>

                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="../media/gallery/bg-signup.jpg" data-image-id="" data-toggle="modal" data-title="https://www.instagram.com/apasaric_photography/"
                        data-image="../media/gallery/bg-signup.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/bg-signup.jpg"
                            alt="Miniatura de la sección de suscripción para la noche">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="../media/gallery/bg-signup_day.jpg" data-image-id="" data-toggle="modal" data-title="https://pixabay.com/users/vuminhld-13924376/"
                        data-image="../media/gallery/bg-signup_day.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/bg-signup_day.jpg"
                            alt="Miniatura de la sección de suscripción para el día">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="../media/gallery/gallery.jpg" data-image-id="" data-toggle="modal" data-title="https://pixabay.com/users/free-photos-242387/"
                        data-image="../media/gallery/gallery.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/gallery.jpg"
                            alt="Miniatura de la Galería">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="https://www.instagram.com/apasaric_photography/"
                        data-image="../media/gallery/game.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/game.jpg"
                            alt="Miniatura del Juego">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="https://maddydesignblog.blogspot.com/"
                        data-image="../media/gallery/logo-gallery.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/logo-gallery.jpg"
                            alt="Logo Midnight Ramen">
                    </a>
                </div>
                <div class="col-lg-3 col-md-4 col-xs-6 thumb">
                    <a class="thumbnail" href="#" data-image-id="" data-toggle="modal" data-title="https://www.instagram.com/apasaric_photography/"
                        data-image="../media/gallery/wiki.jpg"
                        data-target="#image-gallery">
                        <img class="img-thumbnail"
                            src="../media/gallery/wiki.jpg"
                            alt="Miniatura del Glosario">
                    </a>
                </div>
            </div>
            <!-- Gallery Row End -->
            <!-- Gallery Button Start -->
            <div class="modal fade" id="image-gallery" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h4 class="modal-title" id="image-gallery-title"></h4>
                            <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span><span class="sr-only">Close</span>
                            </button>
                        </div>
                        <div class="modal-body">
                            <img id="image-gallery-image" class="img-responsive col-md-12" src="">
                        </div>
                        <div class="modal-footer">
                            <button type="button" class="btn btn-secondary float-left" id="show-previous-image"><i class="fa fa-arrow-left"></i>
                            </button>

                            <button type="button" id="show-next-image" class="btn btn-secondary float-right"><i class="fa fa-arrow-right"></i>
                            </button>
                        </div>
                    </div>
                </div>
            </div>
            <!-- Gallery Button End -->
	    </div>
    </div>
    </section>
    <!-- Gallery Section End -->

    <?php
        include __DIR__."/partials/end-doc.part.php";
    ?>

    <!-- Bootstrap core JavaScript -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

    <!-- Plugin JavaScript -->
    <script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

    <!-- The handler of the gallery -->
    <script src='../js/galleryHandler.js'></script>

</body>
</html>