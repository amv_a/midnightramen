<?php
include __DIR__ . "/partials/ini-doc.part.php";

//JS that changes the images of the header and send section
echo "<script src='js/changeTheme.js'></script>";

include __DIR__ . "/partials/nav.part.php";
?>
<!-- Stories Selector Section -->
<section id="projects" class="projects-section bg-light">
    <div class="container mt-4">
        <div class="row">
                <div class="container "id="stories">
                    <a href="../media/stories/和.pdf" id="story" title="和 (Hén)"><img src="../media/stories/icon.png"></img></a>
                </div>

        </div>
        <div id="storyContainer" class="container">
        </div>
    </div>
</section>

<?php
include __DIR__ . "/partials/end-doc.part.php";
?>

<!-- Bootstrap core JavaScript -->
<script src="../vendor/jquery/jquery.min.js"></script>
<script src="../vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

<!-- Plugin JavaScript -->
<script src="../vendor/jquery-easing/jquery.easing.min.js"></script>

<script src="../js/loadStory.js"></script>

<!-- Custom scripts for this template -->
<script src="../js/grayscale.min.js"></script>

</body>

</html>