<!-- Signup Section Begins -->
<section id="signup" class="signup-section">
  <div class="container">
    <div class="row">
      <div class="col-md-10 col-lg-8 mx-auto text-center">

        <i class="far fa-paper-plane fa-2x mb-2 text-white"></i>
        <h2 class="text-white mb-5">Subscribete para recibir actualizaciones!</h2>

        <form class="form-inline d-flex">
          <input type="email" class="form-control flex-fill mr-0 mr-sm-2 mb-3 mb-sm-0" id="inputEmail" placeholder="Introduce tu dirección de email...">
          <button type="submit" id="submit" class="btn btn-primary mx-auto">¡Subscribete!</button>
        </form>
      </div>
    </div>
  </div>
</section>
<!-- Signup Section Ends -->
<!-- Contact Section Begins-->
<section class="contact-section bg-black">
  <div class="container">
    <div class="social d-flex justify-content-center">
      <a href="#" class="mx-2">
        <i class="fab fa-twitter"></i>
      </a>
      <a href="#" class="mx-2">
        <i class="fab fa-youtube"></i>
      </a>
      <a href="#" class="mx-2">
        <i class="fab fa-github"></i>
      </a>
      <a href="#" class="mx-2">
        <i class="fas fa-envelope"></i>
      </a>
      <a href="#" class="mx-2">
        <i class="fa fa-twitch"></i>
      </a>
      <a href="https://www.youtube.com/watch?v=-bzWSJG93P8" class="mx-2">
        <i class="fa fa-ge"></i>
      </a>
    </div>
  </div>
</section>
<!-- Contact Section Ends-->
<!-- Footer Begins -->
<footer class="bg-black small text-center text-white-50">
  <div class="container">
    <p>Las imagenes que aparecen en esta web son creative commons.</p>
      Copyright &copy; Midnight Ramen 2019
  </div>
</footer>
<!-- Footer Ends -->
