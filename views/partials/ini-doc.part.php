<?php
  include "./utils/utils.php";
?>

<!DOCTYPE html>
<html lang="es">

<head>

  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="title" content="Midnight Ramen Game">
  <meta name="description" content="Text based navigator game">
  <meta name="keywords"
    content="game, navigator game, turn-based combat, text-based wiki, juego, juego de navegador, combate por turnos, juego basado en texto">
  <meta name="robots" content="index, follow">
  <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
  <meta name="language" content="Español">
  <meta charset="UTF-8">
  <link rel="icon" id="favicon" href="favicon.ico" type="image/x-icon" />
  <link rel="shortcut icon" id="faviconShort" href="favicon.ico" type="image/x-icon" />
  <meta name="Aitor Magnieto Ventura" content="">

  <title>Midnight Ramen | <?=$title?></title>

  <!-- Bootstrap core CSS -->
  <link href="vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

  <!-- Custom fonts for this template -->
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet">
  <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Varela+Round" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">

  <!-- JS to check which navigator its being used-->
  <script src="js/faviconChanger.js"></script>
  <!--This will call to a method that adds the styles and scripts depending on the page the user is in-->
  <?= pageRefSelector() ?>
</head>

<body id="page-top">
