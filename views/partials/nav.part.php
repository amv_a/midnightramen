<!-- Navigation -->
<nav class="navbar navbar-expand-lg navbar-light fixed-top" id="mainNav">
    <div class="container">
      <button class="navbar-toggler navbar-toggler-right" type="button" data-toggle="collapse" data-target="#navbarResponsive" aria-controls="navbarResponsive" aria-expanded="false" aria-label="Toggle navigation">
        Menu
        <i class="fas fa-bars"></i>
      </button>
      <?=navIcon()?>
      <div class="collapse navbar-collapse" id="navbarResponsive">
        <ul class="navbar-nav mx-auto">
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" id="mainPage" href="index.php">Página Principal</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" id="wiki" href="glosario.php">Glosario</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" id="gallery" href="galeria.php">Galería</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" id="chgLog" href="versiones.php">Versiones</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" id="toCome" href="futurasVersiones.php">Futuras Actualizaciones</a>
          </li>
          <li class="nav-item">
            <a class="nav-link js-scroll-trigger" id="bugs" href="errores.php">Errores</a>
          </li>
        </ul>
      </div>
    </div>
  </nav>